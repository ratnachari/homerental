//create mini app to handle user module
const exp = require('express');
var userroutes = exp.Router();
//install & import bcrypy
const bcrypt = require('bcrypt');
//install and import jwt
const jwt = require('jsonwebtoken');
const secretKey = "secret";
//importing dbconfig file
const initDb = require('../DBconfig').initDb;
const getDb = require('../DBconfig').getDb;
//importing nodemailer
const nodemailer = require('nodemailer');
const accountSid = 'xxxxxxxxxxxxxxxxxxxxxxxxxxx';
const authToken = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
const client = require('twilio')(accountSid, authToken);
initDb();
//owner routes handelers
userroutes.post('/register', (req, res, next) => {
    console.log(req.body);
    var dbo = getDb();
    if (req.body.user == 'owner') {
        let transporter = nodemailer.
            createTransport({
                service: "gmail",
                auth: {
                    user: "ratnachari492@gmail.com",
                    pass: "xxxxxx"
                }
            });
        let info = transporter.sendMail({
            //sender address
            from: '"login details" <ratnachari492@gmail.com>',
            //list of recivers
            to: req.body.email,
            subject: "owner credentials",//subject line
            text: `username: ${req.body.username},password: ${req.body.upassword}`,//plain text body
            //html:"<b>hiii ra praveen</b>"//htmlbody
        });
        dbo.collection('owner').find({ username: { $eq: req.body.username } }).toArray((err, dataArray) => {
            if (dataArray.length == 0) {
                //hash the password req.body.password
                bcrypt.hash(req.body.upassword, 5, (err, hashedpassword) => {
                    //replace plane text password to hashed password
                    req.body.upassword = hashedpassword;
                    dbo.collection('owner').insertOne(req.body, (err, success) => {
                        if (err) {
                            console.log("error in save operation");
                            console.log(err);
                        }
                        else {
                            res.json({ "message": "successfully registered" })
                        }
                    })
                })
            }
            else {
                res.json({ "message": "name is already exist" })
            }
        })
    }
    else {
        let transporter = nodemailer.
            createTransport({
                service: "gmail",
                auth: {
                    user: "ratnachari492@gmail.com",
                    pass: "xxxxxx"
                }
            });
        let info = transporter.sendMail({
            //sender address
            from: '"login details" <ratnachari492@gmail.com>',
            //list of recivers
            to: req.body.email,
            subject: "vendor credentials",//subject line
            text: `username: ${req.body.username},password: ${req.body.upassword}`,//plain text body
            //html:"<b>hiii ra praveen</b>"//htmlbody
        });
        dbo.collection('vendor').find({ username: { $eq: req.body.username } }).toArray((err, dataArray) => {
            if (dataArray.length == 0) {
                //hash the password req.body.password
                bcrypt.hash(req.body.upassword, 5, (err, hashedpassword) => {
                    //replace plane text password to hashed password
                    req.body.upassword = hashedpassword;
                    dbo.collection('vendor').insertOne(req.body, (err, success) => {
                        if (err) {
                            console.log("error in save operation");
                            console.log(err);
                        }
                        else {
                            res.json({ "message": "successfully registered" })
                        }
                    })
                });
            }
            else {
                res.json({ "message": "name is already exist" })
            }
        });
    }
});
//owner routes handelers
userroutes.post('/login', (req, res, next) => {
    console.log(req.body);
    var dbo = getDb();
    if (req.body.user == 'owner') {
        dbo.collection('owner').find({ username: { $eq: req.body.name } }).toArray((err, dataArray) => {
            console.log(dataArray);
            if (dataArray.length == 0) {
                res.json({ message: "Invalid owner name" })
            }
            else {
                bcrypt.compare(req.body.password, dataArray[0].upassword, (err, result) => {
                    if (result == true) {
                        //create and send json token
                        const signedtoken = jwt.sign({ name: dataArray[0].name }, secretKey, { expiresIn: "7d" });
                        console.log(signedtoken);
                        //sent signed token after successful login
                        res.json({ message: "owner success", token: signedtoken, userdata: dataArray })
                    }
                    else {
                        res.json({ message: "Invalid owner password" })
                    }
                })
            }

        })
    }
    else {
        dbo.collection('vendor').find({ username: { $eq: req.body.name } }).toArray((err, dataArray) => {
            console.log(dataArray);
            if (dataArray.length == 0) {
                res.json({ message: "Invalid vendor name" })
            }
            else {
                bcrypt.compare(req.body.password, dataArray[0].upassword, (err, result) => {
                    if (result == true) {
                        //create and send json token
                        const signedtoken = jwt.sign({ name: dataArray[0].name }, secretKey, { expiresIn: "7d" });
                        console.log(signedtoken);
                        //sent signed token after successful login
                        res.json({ message: "vendor success", token: signedtoken, userdata: dataArray })
                    }
                    else {
                        res.json({ message: "Invalid vendor password" })
                    }
                })
            }

        })
    }
})
//otp verification  
userroutes.post('/forgotpassword', (req, res, next) => {
    console.log(req.body)
    var dbo = getDb();
    if (req.body.user == 'owner') {
        dbname = 'owner'
    }
    else {
        dbname = 'vendor'
    }
    dbo.collection(dbname).find({ username: req.body.username }).toArray((err, userArray) => {
        if (err) {
            next(err)
        }
        else {
            if (userArray.length === 0) {
                res.json({ message: "user not found" })
            }
            else {

                jwt.sign({ username: userArray[0].username }, secretKey, { expiresIn: '7d' }, (err, token) => {
                    if (err) {
                        next(err);
                    }
                    else {
                        var OTP = Math.floor(Math.random() * 99999) + 11111;
                        console.log(OTP)

                        client.messages.create({
                            body: OTP,
                            from: '+19386661281', // From a valid Twilio number
                            to: '+91' + userArray[0].number,  // Text this number

                        })
                            .then((message) => {
                                dbo.collection('OTPCollection').insertOne({
                                    OTP: OTP,
                                    username: userArray[0].username,
                                    OTPGeneratedTime: new Date().getTime() + 60000
                                }, (err, success) => {
                                    if (err) {
                                        next(err)
                                    }
                                    else {
                                        res.json({
                                            "message": "user found",
                                            "token": token,
                                            "OTP": OTP,
                                            "username": userArray[0].username
                                        })
                                    }
                                })
                            });

                    }

                })
            }
        }
    })
})
//verify OTP
userroutes.post('/otp', (req, res, next) => {
    console.log(req.body)
    dbo = getDb();
    console.log(new Date().getTime())
    var currentTime = new Date().getTime()
    dbo.collection('OTPCollection').find({ "OTP": req.body.OTP }).toArray((err, OTPArray) => {
        if (err) {
            next(err)
        }
        else if (OTPArray.length === 0) {
            res.json({ "message": "invalidOTP" })
        }
        else if (OTPArray[0].OTPGeneratedTime < req.body.currentTime) {
            res.json({ "message": "invalidOTP" })
        }
        else {

            dbo.collection('OTPCollection').deleteOne({ OTP: req.body.OTP }, (err, success) => {
                if (err) {
                    next(err);
                }
                else {
                    console.log(OTPArray)
                    res.json({ "message": "verifiedOTP" })
                }
            })
        }
    })
})
//changing password
userroutes.put('/changepassword', (req, res, next) => {
    console.log(req.body)
    dbo = getDb();
    if (req.body.user == 'owner') {
        dbname = 'owner'
    }
    else {
        dbname = 'vendor'
    }
    bcrypt.hash(req.body.password, 5, (err, hashedpassword) => {
        if (err) {
            next(err)
        } else {
            console.log(hashedpassword)
            dbo.collection(dbname).updateOne({ username: req.body.username }, {
                $set: {
                    upassword: hashedpassword
                }
            }, (err, success) => {
                if (err) {
                    next(err)
                }
                else {
                    res.json({ "message": "password changed" })
                }
            })
        }
    })

})


//import error handling
userroutes.use((err, req, res, next) => {
    console.log(err);
})

module.exports = userroutes;