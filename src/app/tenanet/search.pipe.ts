import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(houses: any[], searchWord: string): any {
    if (!searchWord) {
      return houses;
    }
    else {
      return houses.filter(searchedElement =>
        searchedElement.house.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
        searchedElement.address.toLowerCase().indexOf(searchWord.toLowerCase()) != -1 ||
        searchedElement.rent.toString().indexOf(searchWord.toLowerCase()) != -1
      )
    }
  }

}
