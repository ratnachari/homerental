import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TenanetRoutingModule } from './tenanet-routing.module';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { WhoomtoletComponent } from './whoomtolet/whoomtolet.component';
import { PaymentsComponent } from './payments/payments.component';
import { DopayComponent } from './payments/dopay/dopay.component';
import { NavComponent } from './payments/nav/nav.component';

import { EditComponent } from './profile/edit/edit.component';
import { ViprofileComponent } from './profile/viprofile/viprofile.component';
import { PaymenthistoryComponent } from './payments/paymenthistory/paymenthistory.component';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';


@NgModule({
  declarations: [HomeComponent, ProfileComponent, WhoomtoletComponent, PaymentsComponent, DopayComponent, NavComponent, EditComponent, ViprofileComponent, PaymenthistoryComponent, SearchPipe],
  imports: [
    CommonModule,
    TenanetRoutingModule,
    FormsModule
  ]
})
export class TenanetModule { }
