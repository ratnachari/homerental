import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';


@Component({
  selector: 'app-dopay',
  templateUrl: './dopay.component.html',
  styleUrls: ['./dopay.component.css']
})
export class DopayComponent implements OnInit {

  constructor(private ds: HttpClient, private register: RegisterService) { }

  ngOnInit() {
  }
  dopay(data) {
    if (data.owner == "" || data.address1 == "" || data.month == "" || data.amount == "" || data.acno == "" || data.code == "") {
      alert("all fields are mandatory")
    }
    else {
      //console.log(data)
      data.vendorname = this.register.currentuser[0].username;
      data.paystatus = "paid"
      this.ds.post('/home/dopay', data).subscribe((res) => {
        alert(res["message"])
      });
    }
  }
}
