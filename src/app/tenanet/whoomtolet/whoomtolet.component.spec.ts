import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhoomtoletComponent } from './whoomtolet.component';

describe('WhoomtoletComponent', () => {
  let component: WhoomtoletComponent;
  let fixture: ComponentFixture<WhoomtoletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhoomtoletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhoomtoletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
