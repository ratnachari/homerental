import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViprofileComponent } from './viprofile.component';

describe('ViprofileComponent', () => {
  let component: ViprofileComponent;
  let fixture: ComponentFixture<ViprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
