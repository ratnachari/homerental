import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HouseService {

  constructor(private http:HttpClient) { }
  request():Observable<any[]>
  {
    return this.http.get<any[]>('dashboard/viewhouse');
  }
  read():Observable<any[]>
  {
    return this.http.get<any[]>('dashboard/viewpayments');
  }
  history():Observable<any[]>
  {
    return this.http.get<any[]>('home/paymentshistory');
  }
  pay():Observable<any[]>
  {
    return this.http.get<any[]>('home/viewpaymentshistory');
  }
}
