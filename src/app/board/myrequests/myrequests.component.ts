import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-myrequests',
  templateUrl: './myrequests.component.html',
  styleUrls: ['./myrequests.component.css']
})
export class MyrequestsComponent implements OnInit {
  constructor(private http: HttpClient, private register: RegisterService) { }
  currentuser: any;
  ngOnInit() {
    this.http.get(`dashboard/myrequests/${this.register.currentuser[0].username}`).subscribe(res => {
      this.currentuser = res['data'];
    })
    console.log(this.currentuser)
    console.log(this.register.currentuser[0].username)
  }

}
