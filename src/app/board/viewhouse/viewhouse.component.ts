import { Component, OnInit } from '@angular/core';
import { HouseService } from 'src/app/house.service';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-viewhouse',
  templateUrl: './viewhouse.component.html',
  styleUrls: ['./viewhouse.component.css']
})
export class ViewhouseComponent implements OnInit {
  house: any;

  constructor(private hc: RegisterService, private http: HttpClient) { }

  ngOnInit() {
    var username = this.hc.currentuser[0].username
    this.http.get(`/dashboard/viewhouse/${username}`).subscribe(house => {
      if (house['message'] == 'unauthorized access') {
        alert(house['message'])
      }
      else {
        this.house = house['message']
      }
    });
    console.log(this.house)
  }
  delete(address) {
    this.http.delete(`/dashboard/delete/${address}`).subscribe(res => {
      alert(res['message']);
      this.house = res['data']
    })
  }
}




