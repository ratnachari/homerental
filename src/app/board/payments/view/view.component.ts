import { Component, OnInit } from '@angular/core';
import { HouseService } from 'src/app/house.service';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  payment: any[];
  constructor(private hc: RegisterService, private http: HttpClient) { }

  ngOnInit() {
    var username = this.hc.currentuser[0].username
    this.http.get(`/dashboard/viewpayments/${username}`).subscribe(res => {
      this.payment = res['message']

    });
    //console.log(this.payment)
  }

}
