import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarouselComponent } from './carousel/carousel.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavComponent } from './nav/nav.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { OtpComponent } from './otp/otp.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'nav/carousel',
    pathMatch: 'full'
  },
  {
    path: 'nav',
    component: NavComponent,
    children: [{
      path: 'carousel',
      component: CarouselComponent
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'forgotpassword',
      component: ForgotpasswordComponent
    },
    {
      path: 'otp',
      component: OtpComponent
    },
    {
      path: 'changepassword',
      component: ChangepasswordComponent
    },
    {
      path: 'contactus',
      component: ContactusComponent
    },
    { path: 'admin', loadChildren: () => import('./admin/admin.module') },
    { path: 'board', loadChildren: () => import('./board/board.module') },
    { path: 'tenanet', loadChildren: () => import('./tenanet/tenanet.module') }


    ]
  }]
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
